package filemanager;

import java.util.List;

public interface FileManager<T> {
	public List<T> read(String filePath);

}
