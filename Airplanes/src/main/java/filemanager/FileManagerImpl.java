package filemanager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public abstract class FileManagerImpl<T> implements FileManager<T> {

	
	public List<T> read(String filePath) {
		List<T> result = new LinkedList<>();
		try (BufferedReader in = new BufferedReader(new FileReader(filePath))) {
			String line;
			while ((line = in.readLine()) != null) {
				T t = createInstance(line);
				if (t != null) {
					result.add(t);
				}
			}
		} catch (IOException e) {
			System.err.println("Error while reading file" + filePath);
		}
		return result;
	}

	public abstract T createInstance(String line);

}
