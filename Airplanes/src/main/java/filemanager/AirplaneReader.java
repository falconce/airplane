package filemanager;

import model.Airplane;

public class AirplaneReader extends FileManagerImpl<Airplane> {

	@Override
	public Airplane createInstance(String line) {
		if (line == null || line.isEmpty()) {
			return null;
		}

		String[] tokens = line.split(",");

		if (tokens.length != 5) {
			return null;
		}

		return new Airplane(tokens[0].trim(), tokens[1].trim(), tokens[2].trim(), tokens[3].trim(), tokens[4].trim());
	}

}
