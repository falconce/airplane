package filemanager;


import model.PData;

public class DataReader extends FileManagerImpl<PData> {
	
	@Override
	public PData createInstance(String line) {
		if (line == null || line.isEmpty()) {
			return null;
		}
		String id = line.substring(0, 1);
		String passengers = line.substring(1, 4);
		String fuel = line.substring(4, 8);
		String result = id + " " + passengers + " " + fuel;
		String[] tokens = result.split(" ");

		

		
		
		return new PData(tokens[0].trim(), tokens[1].trim(), tokens[2].trim());

		
	}

}
