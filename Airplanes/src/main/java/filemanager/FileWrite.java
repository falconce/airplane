package filemanager;

import static engine.ParserCSV.result;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWrite {
	public static final String DESTINATION = "./Output/Output.csv";
	
	
	public void writeFile() {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(DESTINATION)))) {
			writer.write(result);
		} catch (IOException e) {
			System.out.println("Problem writing file.");
			e.printStackTrace();
		}
	}
		

	
}
