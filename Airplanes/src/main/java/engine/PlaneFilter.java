package engine;

import java.util.LinkedList;
import java.util.List;

import calculator.CalcPercentage;
import filemanager.AirplaneReader;
import filemanager.DataReader;
import filemanager.FileManager;
import model.Airplane;
import model.PData;

public class PlaneFilter {
	FileManager<Airplane> airp = new AirplaneReader();
	List<Airplane> alist = airp.read("./Input/InputCSV.csv");
	
	FileManager<PData> datap = new DataReader();
	List<PData> dlist = datap.read("./Input/InputFW.txt");
	
	CalcPercentage calculate = new CalcPercentage();
	
	public static List<Airplane> result = new LinkedList<>();

	public void filter() {
		for (int i = 0; i <= 3; i++) {
			float passengerPerc = calculate.calcPercent(Float.valueOf(dlist.get(i).getPassengers()), Float.valueOf(alist.get(i).getTotalseats()));
			float fuelPerc = calculate.calcPercent(Float.valueOf(dlist.get(i).getFuel()), Float.valueOf(alist.get(i).getMaxfuel()));
			if (passengerPerc >= 80.0f && fuelPerc >= 70.0f) {
				result.add(alist.get(i));
			}
			else {
				continue;
			}
		}
	}
	/*public void printlist() {
		for (Airplane i : result) {
			System.out.println(i);
		} 
	} */
}
