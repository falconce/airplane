package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Airplane {
	private String id;
	private String manufacturer;
	private String model;
	private String totalseats;
	private String maxfuel;
	
	
	@Override
	public String toString() {
		return "Airplane [id=" + id + ", manufacturer=" + manufacturer + ", model=" + model + ", totalseats="
				+ totalseats + ", maxfuel=" + maxfuel + "]";
	}


	
	
}
