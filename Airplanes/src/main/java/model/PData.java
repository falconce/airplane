package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PData {
	
	private String id;
	private String passengers;
	private String fuel;
	
	@Override
	public String toString() {
		return "PData [id=" + id + ", passengers=" + passengers + ", fuel=" + fuel + "]";
	}

}
